import Vue from 'vue'
import Router from 'vue-router'
import principal from '@/components/principal'
import info_juego from '@/components/info_juego'
import agregar_juego from '@/components/agregar_juego'
import editar_juego from '@/components/editar_juego'
import info_des from '@/components/info_des'
import agregar_des from '@/components/agregar_des'
import editar_des from '@/components/editar_des'
import crear_usuario from '@/components/crear_usuario'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'principal',
      component: principal
    },
    {
      path: '/info_juego/:juego_id',
      name: 'info_juego',
      component: info_juego
    },
    {
      path: '/agregar_juego',
      name: 'agregar_juego',
      component: agregar_juego
    },
    {
      path: '/editar_juego/:id_juego',
      name: 'editar_juego',
      component: editar_juego
    },
    {
      path: '/info_des/:des_id',
      name: 'info_des',
      component: info_des
    },
    {
      path: '/agregar_des',
      name: 'agregar_des',
      component: agregar_des
    },
    {
      path: '/editar_des/:id_des',
      name: 'editar_des',
      component: editar_des
    },
    {
      path: '/crear_usuario',
      name: 'crear_usuario',
      component: crear_usuario
    }
  ]
})