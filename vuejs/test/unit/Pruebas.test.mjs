import Vue from 'vue'
import { shallowMount } from '@vue/test-utils'
import agregar_des from '@/components/agregar_des'
import agregar_juego from '@/components/agregar_juego'
import crear_usuario from '@/components/crear_usuario'
import det_juego from '@/components/det_juego'
import editar_des from '@/components/editar_des'
import editar_juego from '@/components/editar_juego'
import info_des from '@/components/info_des'
import info_juego from '@/components/info_juego'
import login from '@/components/login'
import principal from '@/components/principal'

describe('agregar_des.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(agregar_des)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('Agregar un nuevo desarrollador correctamente', () => {
    const nuevo_des = { nombre: 'Prueba', 
			descripcion: 'Esto es una prueba',
			pais: 'cualquiera'}
    cmp.vm.login('kevinalpizar1@gmail.com', 'alpizar11')
    expect(cmp.vm.agregar_des()).toBeTruthy()
  })
})

describe('agregar_juego.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(agregar_juego)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('crear_usuario.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(crear_usuario)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('det_juego.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(det_juego)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('editar_des.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(editar_des)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('editar_juego.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(editar_juego)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('info_des.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(info_des)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('info_juego.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(info_juego)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('login.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(login)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})

describe('principal.vue', () => {
  let cmp

  beforeEach(() => {
    cmp = shallowMount(principal)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should render correct contents', () => {
    
  })
})
